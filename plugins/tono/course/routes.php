<?php

use Tono\Course\Models\Course;

Route::get('courses', function () {
    $courses = Course::all();
    return $courses;
});