<?php namespace Tono\Course\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTonoCourseCourses extends Migration
{
    public function up()
    {
        Schema::create('tono_course_courses', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->text('desctiption');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tono_course_courses');
    }
}
