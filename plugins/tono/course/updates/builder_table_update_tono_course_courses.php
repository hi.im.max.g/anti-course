<?php namespace Tono\Course\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTonoCourseCourses extends Migration
{
    public function up()
    {
        Schema::table('tono_course_courses', function($table)
        {
            $table->renameColumn('desctiption', 'description');
        });
    }
    
    public function down()
    {
        Schema::table('tono_course_courses', function($table)
        {
            $table->renameColumn('description', 'desctiption');
        });
    }
}
